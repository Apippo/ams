<div style="display:flex;justify-content:center;align-items:stretch;flex-direction:row">
<table style="width:100%;text-align:center;font-size:200%">
<tr>
<th rowspan="3">

Parent state

</th>
<th style="font-size:80%;color:#1591ea">Lens</th>
<th rowspan="3">

Child state

</th>
</tr>
<tr>
<td style="color:#1591ea">

get:

`parentState => childState`

</td>
</tr>
<tr>

<td style="color:#1591ea">

set:

`(newChild, oldParent) => newParent`

</td>
</tr>

</table>
</div>