import {Set} from 'immutable';

function f(x) {
  if (x === 1) return 'one';
  if (x === 2) return 'two';
  if (x === 3) return 'three';
  return '?';
}

const numbers = [1, 2];
const strings = numbers.map(f);
console.log(strings); // ['one', 'two']

const uniqueNums = Set.of(1, 2, 3);
const uniqueStrs = uniqueNums.map(f);
console.log(uniqueStrs); // Set: "one", "two", "three"
