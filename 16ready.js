const R = require('ramda')

function fToC(fahrenheit) {
  return Math.round(((fahrenheit - 32) * 5) / 9);
}

function cToF(celsius) {
  return Math.round((celsius * 9) / 5 + 32);
}

const weather = {
  temp: 80,
  wind: 42,
}

const pickTempLens = R.lens(
  obj => obj.temp,
  (temp, obj) => ({...obj, temp})
);
const celsiusLens = R.lens(fToC, cToF);
const lens = R.compose(pickTempLens, celsiusLens);

console.log('before', weather);
console.log('in celsius', R.view(lens, weather));
console.log('new', R.set(lens, 20, weather));
console.log('after', weather);
