<div style="font-size:200%">

### Lenses for state management

- Feels like local state, but is global state
- Serializable, immutable, functional, fractal
- Less concepts (~~reducers, actions, store, dispatch~~)
- If you know selectors, you can learn lenses

</div>