<div style="text-align:center;">

## This

```mermaid
graph TD

  A-->B
  A-->C
  B-->B1
  B-->B2
  B-->B3
  C-->C1
  C-->C2
  C-->C3

  classDef blue fill:#97cbfc,stroke:#333,stroke-width:1px;

  class A,B,C,B1,B2,B3,C1,C2,C3 blue;

  linkStyle 0 stroke:#000,stroke-width:1px;
  linkStyle 1 stroke:#000,stroke-width:1px;
  linkStyle 2 stroke:#000,stroke-width:1px;
  linkStyle 3 stroke:#000,stroke-width:1px;
  linkStyle 4 stroke:#000,stroke-width:1px;
  linkStyle 5 stroke:#000,stroke-width:1px;
  linkStyle 6 stroke:#000,stroke-width:1px;
  linkStyle 7 stroke:#000,stroke-width:1px;
```

## but without mutating parts to synchronize

</div>

