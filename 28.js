import Vue from 'vue/dist/vue.esm';
import * as lens from 'vue-lens-mixin';
const {fToC, cToF} = require('./utils')

Vue.component('us-thermo', {
  mixins: [lens],
  template: `
    <div class="american">
      <h1>American thermometer</h1>
      <button v-on:click="state -= 5">Colder</button>
      <button v-on:click="state += 5">Hotter</button>
      <h1>{{state}}°F</h1>
    </div>
  `,
});

Vue.component('eu-thermo', {
  mixins: [lens],
  template: `
    <div class="european">
      <h1>European thermometer</h1>
      <button v-on:click="state -= 2">Colder</button>
      <button v-on:click="state += 2">Hotter</button>
      <h1>{{state}}°C</h1>
    </div>
  `,
});

new Vue({
  data: function() {
    return {
      state: {temp: 80, wind: 42},
      usLens: {
        get: obj => obj.temp,
        set: (temp, obj) => ({...obj, temp}),
      },
      euLens: {
        get: obj => fToC(obj.temp),
        set: (c, obj) => ({...obj, temp: cToF(c)}),
      },
    };
  },
  template: `
    <div id="app">
      <span>Global state: {{JSON.stringify(state)}}</span>
      <us-thermo v-bind:lens="usLens"/>
      <eu-thermo v-bind:lens="euLens"/>
    </div>
  `,
}).$mount('#root');
