import React from 'react';
import ReactDOM from 'react-dom';
import useProfunctorState from '@staltz/use-profunctor-state';
const {fToC, cToF} = require('./utils')

function USThermo({state, setState}) {
  const onColder = () => setState(f => f - 5);
  const onHotter = () => setState(f => f + 5);
  return (
    <div className={'american'}>
      <h1>American thermometer</h1>
      <button onClick={onColder}>Colder</button>
      <button onClick={onHotter}>Hotter</button>
      <h1>{state}°F</h1>
    </div>
  );
}

function EUThermo({state, setState}) {
  const onColder = () => setState(c => c - 2);
  const onHotter = () => setState(c => c + 2);
  return (
    <div className={'european'}>
      <h1>European thermometer</h1>
      <button onClick={onColder}>Colder</button>
      <button onClick={onHotter}>Hotter</button>
      <h1>{state}°C</h1>
    </div>
  );
}

function App() {
  const profunctor = useProfunctorState({temp: 80, wind: 42});

  const usLens = profunctor
    .promap(
      obj => obj.temp,
      (temp, obj) => ({...obj, temp}),
    );

  const euLens = profunctor
    .promap(
      obj => obj.temp,
      (temp, obj) => ({...obj, temp}),
    )
    .promap(fToC, cToF);

  return (
    <div>
      <span>Global state: {JSON.stringify(profunctor.state)}</span>
      <USThermo {...usLens} />
      <EUThermo {...euLens} />
    </div>
  );
}

const rootElement = document.getElementById('root');
ReactDOM.render(<App />, rootElement);
