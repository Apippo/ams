import xs from 'xstream';
import {run} from '@cycle/run';
import isolate from '@cycle/isolate';
import {withState} from '@cycle/state';
import {makeDOMDriver, div, span, button, h1} from '@cycle/dom';
const {fToC, cToF} = require('./utils');

function USThermo(sources) {
  const updater$ = xs.merge(
    xs.of(prev => (prev ? prev : 50)),
    sources.DOM.select('.colder').events('click').mapTo(f => f - 5),
    sources.DOM.select('.hotter').events('click').mapTo(f => f + 5),
  );

  const dom$ = sources.state.stream.map(fahrenheit =>
    div('.american', [
      h1('American thermometer'),
      button('.colder', 'Colder'),
      button('.hotter', 'Hotter'),
      h1(fahrenheit + '°F'),
    ]),
  );

  return {
    DOM: dom$,
    state: updater$,
  };
}

function EUThermo(sources) {
  const updater$ = xs.merge(
    xs.of(prev => (prev ? prev : 20)),
    sources.DOM.select('.colder').events('click').mapTo(c => c - 2),
    sources.DOM.select('.hotter').events('click').mapTo(c => c + 2),
  );

  const dom$ = sources.state.stream.map(celsius =>
    div('.european', [
      h1('European thermometer'),
      button('.colder', 'Colder'),
      button('.hotter', 'Hotter'),
      h1(celsius + '°C'),
    ]),
  );

  return {
    DOM: dom$,
    state: updater$,
  };
}

function App(sources) {
  const usLens = {
    get: obj => obj.temp,
    set: (parent, temp) => ({...parent, temp}),
  };

  const usthermo = isolate(USThermo, {state: usLens, '*': 'us'})(sources);

  const euLens = {
    get: obj => fToC(obj.temp),
    set: (parent, c) => ({...parent, temp: cToF(c)}),
  };

  const euthermo = isolate(EUThermo, {state: euLens, '*': 'eu'})(sources);

  const dom$ = xs
    .combine(sources.state.stream, usthermo.DOM, euthermo.DOM)
    .map(([state, usDOM, euDOM]) =>
      div([span('Global state: ' + JSON.stringify(state)), usDOM, euDOM]),
    );

  const updater$ = xs.of(prev => (prev ? prev : {temp: 80, wind: 42}));

  return {
    DOM: dom$,
    state: xs.merge(updater$, usthermo.state, euthermo.state),
  };
}

run(withState(App), {
  DOM: makeDOMDriver('#root'),
});
