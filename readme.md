# Functional lenses for contemporary frameworks

> A talk by @andrestaltz at Amsterdam.js 2019

View the "slides" by going through files named by numbers, e.g. `1.md`, `2.jpeg`, `3.md`, ...

## License

[Creative Commons Attribution-NonCommercial 4.0 International License](https://creativecommons.org/licenses/by-nc/4.0/)
