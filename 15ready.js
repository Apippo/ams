function fToC(fahrenheit) {
  return Math.round(((fahrenheit - 32) * 5) / 9);
}

function cToF(celsius) {
  return Math.round((celsius * 9) / 5 + 32);
}

const weather = {
  temp: 80,
  wind: 42,
}

const euWeather = {
  get temp() {
    return fToC(weather.temp);
  },

  set temp(c) {
    weather.temp = cToF(c);
  },
}

console.log(euWeather.temp);
euWeather.temp = 20;
console.log(euWeather.temp);
